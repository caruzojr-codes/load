import { Component, OnInit } from '@angular/core';

import { LoadingService } from './../../shared/services/loading.service';
import { UsuariosService } from './../../shared/services/usuarios.service'

import { Usuario } from './../../shared/models/usuario.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private loadingService: LoadingService,
    private usuariosService: UsuariosService
  ) { }

  ngOnInit() {
    this.loadingService.onLoadingStarted.emit();
    this.getUsuarios();
  }

  getUsuarios() {
    return this.usuariosService.getUsuarios()
      .subscribe((result: Usuario[]) => {
        console.log(result);
        this.loadingService.onLoadingFinished.emit();
      },
      error => console.log(error))
  }

}
