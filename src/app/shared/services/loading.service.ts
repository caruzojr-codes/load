import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  onLoadingFinished = new EventEmitter<any>();
  onLoadingStarted = new EventEmitter<any>();

}
