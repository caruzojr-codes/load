import { Component } from '@angular/core';

import { LoadingService } from './shared/services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public loading: boolean;

  constructor(
    private loadingService: LoadingService
  ) { }

  ngOnInit() {
    this.loadingService.onLoadingStarted.subscribe(() => {
      setTimeout(() => {
        this.loading = true;
      }, 0)
    });

    this.loadingService.onLoadingFinished.subscribe(() => {
      setTimeout(() => {
        this.loading = false;
      }, 0);
    });
  }

}
